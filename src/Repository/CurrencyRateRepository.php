<?php

namespace App\Repository;

use App\Entity\CurrencyRate;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CurrencyRate|null find($id, $lockMode = null, $lockVersion = null)
 * @method CurrencyRate|null findOneBy(array $criteria, array $orderBy = null)
 * @method CurrencyRate[]    findAll()
 * @method CurrencyRate[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CurrencyRateRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CurrencyRate::class);
    }


    public function findLastRate(string $currency, string $type)
    {
        return $this->createQueryBuilder('currency_rate')
            ->andWhere('currency_rate.currency = :currency')
            ->andWhere('currency_rate.bank = :bank')
            ->setParameters([
                'currency' => $currency,
                'bank' => $type,
            ])
            ->orderBy('currency_rate.id', 'desc')
            ->setMaxResults(1)
            ->getQuery()
            ->getResult();
    }

    public function findSellRates(string $currency, string $bank): array
    {
        return $this->findBaseRates('sell', $currency, $bank);
    }

    public function findBuyRates(string $currency, string $bank): array
    {
        return $this->findBaseRates('buy', $currency, $bank);
    }

    public function findBaseRates(string $base, string $currency, string $bank): array
    {
        $query = $this->createQueryBuilder('currency_rate')
            ->andWhere('currency_rate.currency = :currency')
            ->andWhere('currency_rate.bank = :bank')
            ->setParameters([
                'currency' => $currency,
                'bank' => $bank,
            ])
            ->select('currency_rate.created_at as x, currency_rate.' . $base . ' as y')
            ->orderBy('currency_rate.created_at', 'asc')
            ->getQuery();

//        dd($query->getSql());

        return $query
            ->getResult();
    }

    // /**
    //  * @return CurrencyRate[] Returns an array of CurrencyRate objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CurrencyRate
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
