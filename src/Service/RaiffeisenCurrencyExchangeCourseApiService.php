<?php
declare(strict_types=1);

namespace App\Service;

use App\Exception\CurrencyExchangeCourseApiException;
use App\Exception\RaiffeisenCurrencyExchangeCourseApiException;
use DateTime;
use DateTimeInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\RequestOptions;

class RaiffeisenCurrencyExchangeCourseApiService extends CurrencyExchangeCourseApiInterface
{
    protected string $type = 'raiffeisen';
    private string $url = 'https://online.raiffeisen.ru/rest/exchange/open/rate/info';

    /**
     * @return array
     * @throws GuzzleException
     * @throws RaiffeisenCurrencyExchangeCourseApiException
     */
    public function fetchCourses(): array
    {
        $client = new Client();
        $response = $client->get($this->url, [
            RequestOptions::QUERY => [
                'currencySource' => $this->from,
                'currencyDest' => $this->to,
                'scope' => '4',
            ],
        ]);

        $decodedResponse = json_decode((string)$response->getBody());


        if (true === empty($decodedResponse->buyRate) || true === empty($decodedResponse->sellRate)) {
            throw new RaiffeisenCurrencyExchangeCourseApiException('API response result rates is empty');
        }

        return $this->mapApiResponseToApp([$decodedResponse]);
    }

    /**
     * @param array $rates
     * @return array
     */
    private function mapApiResponseToApp(array $rates): array
    {
        $output = [];
        foreach ($rates as $rate) {
            $output[] = [
                'buy' => $rate->buyRate,
                'sell' => $rate->sellRate,
            ];
        }

        return $output;
    }
}
