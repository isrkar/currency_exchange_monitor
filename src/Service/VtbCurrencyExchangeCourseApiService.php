<?php
declare(strict_types=1);

namespace App\Service;

use App\Exception\CurrencyExchangeCourseApiException;
use App\Exception\RaiffeisenCurrencyExchangeCourseApiException;
use App\Exception\VtbCurrencyExchangeCourseApiException;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\RequestOptions;

class VtbCurrencyExchangeCourseApiService extends CurrencyExchangeCourseApiInterface
{
    protected string $type = 'vtb';
    private string $url = 'https://www.vtb.ru/api/currency-exchange/currency-converter-info';

    public function fetchCourses(): array
    {
        $sell = $this->getFromRate();
        $buy = $this->getToRate();

        return [['sell' => $sell, 'buy' => $buy]];
    }

    /**
     * @return mixed
     * @throws GuzzleException
     * @throws VtbCurrencyExchangeCourseApiException
     */
    private function getToRate()
    {
        return $this->getBaseRate($this->from, 'RUR', 'ToRate');
    }

    /**
     * @return mixed
     * @throws GuzzleException
     * @throws VtbCurrencyExchangeCourseApiException
     */
    private function getFromRate()
    {
        return $this->getBaseRate('RUR', $this->from, 'FromRate');
    }

    /**
     * @param $from
     * @param $to
     * @param $target
     * @return mixed
     * @throws GuzzleException
     * @throws VtbCurrencyExchangeCourseApiException
     */
    private function getBaseRate($from, $to, $target)
    {
        $client = new Client();
        $response = $client->get($this->url, [
            RequestOptions::QUERY => [
                'changeSummaType' => 1,
                'contextItemId' => urlencode('{' . uniqid() . '}'),
                'conversionPlace' => 1,
                'conversionType' => 1,
                'currencyFromCode' => $from,
                'currencyToCode' => $to,
                'fromSumma' => 0,
                'toSumma' => 0,
            ],
        ]);

        $decodedResponse = json_decode((string)$response->getBody());

        if (true === empty($decodedResponse->$target)) {
            throw new VtbCurrencyExchangeCourseApiException('API response result rates is empty');
        }

        return $decodedResponse->$target;
    }
}
