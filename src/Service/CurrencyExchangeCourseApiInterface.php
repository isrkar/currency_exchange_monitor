<?php
declare(strict_types=1);

namespace App\Service;

use App\Exception\CurrencyExchangeCourseApiException;
use GuzzleHttp\Exception\GuzzleException;

abstract class CurrencyExchangeCourseApiInterface
{
    protected string $from, $to;
    protected string $type;

    /**
     * @param string $from
     * @param string $to
     */
    public function __construct(string $from = 'USD', string $to = 'RUB')
    {
        $this->from = $from;
        $this->to = $to;
    }

    /**
     * @throws GuzzleException
     * @throws CurrencyExchangeCourseApiException
     * @return array
     */
    abstract public function fetchCourses(): array;

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getFrom(): string
    {
        return $this->from;
    }

    /**
     * @return string
     */
    public function getTo(): string
    {
        return $this->to;
    }
}
