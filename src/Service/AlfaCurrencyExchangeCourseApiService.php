<?php
declare(strict_types=1);

namespace App\Service;

use App\Exception\AlfaCurrencyExchangeCourseApiException;
use App\Exception\CurrencyExchangeCourseApiException;
use DateTime;
use DateTimeInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\RequestOptions;

class AlfaCurrencyExchangeCourseApiService extends CurrencyExchangeCourseApiInterface
{
    protected string $type = 'alfa';
    private string $url = 'https://alfabank.ru/api/v1/scrooge/currencies/alfa-rates';

    const headers = [
        'Accept' => 'application/json, text/plain, */*',
        'User-Agent' => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36',
        'Referer' => 'https://alfabank.ru/currency/',
        'Connection' => 'keep-alive',
    ];

    /**
     * @throws CurrencyExchangeCourseApiException
     * @throws GuzzleException
     */
    public function fetchCourses(): array
    {
        $client = new Client();
        $response = $client->get($this->url, [
            RequestOptions::QUERY => [
                'currencyCode.in' => $this->from,
                'rateType.eq' => 'makeCash',
                'lastActualForDate.eq' => 'true',
                'clientType.eq' => 'standardCC',
                'date.lte' => (new DateTime())->format(DateTimeInterface::RFC3339) // '2022-01-07T09:44:20+03:00',
            ],
            RequestOptions::HEADERS => self::headers
        ]);

        $decodedResponse = json_decode((string)$response->getBody());


        if (false === isset($decodedResponse->data)) {
            throw new AlfaCurrencyExchangeCourseApiException('API response result is not "OK"');
        }

        if (true === empty($decodedResponse->data[0]) || $decodedResponse->data[0]->currencyCode !== $this->from) {
            throw new AlfaCurrencyExchangeCourseApiException('API response result rates is empty');
        }

        return $this->mapApiResponseToApp($decodedResponse->data);
    }

    private function mapApiResponseToApp(array $rates): array
    {
        $rates = array_filter($rates, function ($element): bool {
            if ($this->from === $element->currencyCode) {
                return true;
            }
            return false;
        }, ARRAY_FILTER_USE_BOTH);

        $newRates = [];

        foreach ($rates as $el0) {
            foreach ($el0->rateByClientType as $el1) {
                foreach ($el1->ratesByType as $el2) {
                    $newRates[] = [
                        'buy' => $el2->lastActualRate->buy->originalValue,
                        'sell' => $el2->lastActualRate->sell->originalValue,
                    ];
                }
            }
        }

        return $newRates;
    }
}
