<?php
declare(strict_types=1);

namespace App\Service;

use App\Exception\CurrencyExchangeCourseApiException;
use App\Exception\TinkoffCurrencyExchangeCourseApiException;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\RequestOptions;

class TinkoffCurrencyExchangeCourseApiService extends CurrencyExchangeCourseApiInterface
{
    private string $url = 'https://api.tinkoff.ru/v1/currency_rates';
    private array $allowedCategories = ['CUTransfersPro'];
    const headers = [
        'authority' => 'api.tinkoff.ru',
        'user-agent' => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36',
        'content-type' => 'application/x-www-form-urlencoded',
        'referer' => 'https://www.tinkoff.ru/',
        'accept-language' => 'ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7,de;q=0.6',
    ];

    protected string $type = 'tinkoff';

    /**
     * @return array
     * @throws GuzzleException
     * @throws CurrencyExchangeCourseApiException
     */
    public function fetchCourses(): array
    {
        $client = new Client();
        $response = $client->get($this->url, [
            RequestOptions::QUERY => [
                'from' => $this->from,
                'to' => $this->to,
            ],
            RequestOptions::HEADERS => self::headers
        ]);

        $decodedResponse = json_decode((string)$response->getBody());

        if (false === isset($decodedResponse->resultCode) || $decodedResponse->resultCode !== 'OK') {
            throw new TinkoffCurrencyExchangeCourseApiException('API response result is not "OK"');
        }

        if (true === empty($decodedResponse->payload) || true === empty($decodedResponse->payload->rates)) {
            throw new TinkoffCurrencyExchangeCourseApiException('API response result rates is empty');
        }

        return $this->mapApiResponseToApp($decodedResponse->payload->rates);
    }

    /**
     * @param array $rates
     * @return array
     */
    private function mapApiResponseToApp(array $rates): array
    {
        $rates = array_filter($rates, function ($element): bool {
            if (true === in_array($element->category, $this->allowedCategories)) {
                return true;
            }
            return false;
        }, ARRAY_FILTER_USE_BOTH);

        $rates = array_map(function($el) {
            $newElement['buy'] = (float)$el->buy;
            $newElement['sell'] = (float)$el->sell;
            return $newElement;
        }, $rates);

        return array_merge($rates);
    }
}
