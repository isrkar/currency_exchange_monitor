<?php
declare(strict_types=1);

namespace App\Command;

use App\Entity\CurrencyRate;
use App\Repository\CurrencyRateRepository;
use App\Service\AlfaCurrencyExchangeCourseApiService;
use App\Service\CurrencyExchangeCourseApiInterface;
use App\Service\RaiffeisenCurrencyExchangeCourseApiService;
use App\Service\TinkoffCurrencyExchangeCourseApiService;
use App\Service\VtbCurrencyExchangeCourseApiService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Throwable;

class FetchCoursesCommand extends Command
{
    protected static $defaultName = 'app:fetch_courses';

    /**
     * @var string[]
     */
    private array $servicesList = [
        TinkoffCurrencyExchangeCourseApiService::class,
        AlfaCurrencyExchangeCourseApiService::class,
        RaiffeisenCurrencyExchangeCourseApiService::class,
        VtbCurrencyExchangeCourseApiService::class,
    ];

    /**
     * @var CurrencyExchangeCourseApiInterface[]
     */
    private array $services = [];

    private EntityManagerInterface $entityManager;

    /**
     * @var CurrencyRate[]
     */
    private array $lastRates = [];

    private CurrencyRateRepository $rateRepository;

    public function __construct(EntityManagerInterface $entityManager, CurrencyRateRepository $rateRepository)
    {
        parent::__construct();
        $this->rateRepository = $rateRepository;
        $this->entityManager = $entityManager;
    }

    protected function configure(): void
    {

    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        foreach ($this->servicesList as $serviceClass) {
            if (empty($this->services[$serviceClass])) {
                $this->services[$serviceClass] = new $serviceClass();
            }

            try {
                $rates = $this->services[$serviceClass]->fetchCourses();
            } catch (Throwable $e) {
                $output->writeln('Error while fetching ' . $serviceClass);
                $output->writeln($e->getMessage());
                continue;
            }

            foreach ($rates as $apiRate) {
                // todo вывести эту упоротую логику в отдельный сервис и пользоваться временными переменными для длинных конструкций
                if (empty($this->lastRates[$this->services[$serviceClass]->getFrom() . $this->services[$serviceClass]->getType()])) {
                    $lastRates = $this->rateRepository->findLastRate($this->services[$serviceClass]->getFrom(), $this->services[$serviceClass]->getType());
                    if (false === empty($lastRates[0])) {
                        $this->lastRates[$this->services[$serviceClass]->getFrom() . $this->services[$serviceClass]->getType()] = $lastRates[0];
                    }
                }

                if (
                    (
                        false === empty($this->lastRates[$this->services[$serviceClass]->getFrom() . $this->services[$serviceClass]->getType()])
                        && $this->lastRates[$this->services[$serviceClass]->getFrom() . $this->services[$serviceClass]->getType()]->getBuy() !== $apiRate['buy']
                        && $this->lastRates[$this->services[$serviceClass]->getFrom() . $this->services[$serviceClass]->getType()]->getSell() !== $apiRate['sell']
                    )
                    || (
                        true === empty($this->lastRates[$this->services[$serviceClass]->getFrom() . $this->services[$serviceClass]->getType()])
                    )
                ) {
                    $rate = (new CurrencyRate())
                        ->setBuy($apiRate['buy'])
                        ->setSell($apiRate['sell'])
                        ->setCurrency($this->services[$serviceClass]->getFrom())
                        ->setBank($this->services[$serviceClass]->getType());
                    $this->entityManager->persist($rate);
                    $this->entityManager->flush();
                }


            }

        }

        return Command::SUCCESS;
    }
}
