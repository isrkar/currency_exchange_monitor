<?php
declare(strict_types=1);

namespace App\Controller;

use App\Repository\CurrencyRateRepository;
use App\Service\AlfaCurrencyExchangeCourseApiService;
use App\Service\TinkoffCurrencyExchangeCourseApiService;
use DateTime;
use DateTimeInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\UX\Chartjs\Builder\ChartBuilderInterface;
use Symfony\UX\Chartjs\Model\Chart;

class CabinetController extends AbstractController
{
    /**
     * @Route("/cabinet/", name="cabinet_index")
     */
    public function index(ChartBuilderInterface $chartBuilder, CurrencyRateRepository $repository): Response
    {
        $buy = $repository->findBuyRates('USD', 'tinkoff');
        $sell = $repository->findSellRates('USD', 'tinkoff');

        foreach ($buy as &$b) {
            $b['x'] = $b['x']->format('Y-m-d H:i:s');
        }

        foreach ($sell as &$b) {
            $b['x'] = $b['x']->format('Y-m-d H:i:s');
        }

//        dd($sell);

        $chart = $chartBuilder->createChart(Chart::TYPE_LINE);
        $chart->setData([
            'datasets' => [
                [
                    'label' => 'buy',
                    'backgroundColor' => 'rgb(255, 99, 132)',
                    'borderColor' => 'rgb(255, 99, 132)',
                    'data' => $buy,
                ],
                [
                    'label' => 'sell',
                    'backgroundColor' => 'rgb(180, 255, 132)',
                    'borderColor' => 'rgb(180, 255, 132)',
                    'data' => $sell,
                ],
            ],
        ]);

        $chart->setOptions([
            'scales' => [
                'x' => [
                    'type' => 'time',
                    'time' => [
                        'unit' => 'month',
                    ]
                ],
            ],
        ]);

        return $this->render('sas.html.twig', [
            'chart' => $chart,
        ]);
    }
}
