FROM php:7.4-fpm

RUN apt-get update && apt-get install -y \
    locales \
    libpq-dev \
    cron \
    zip \
    unzip \
    curl \
    libzip-dev \
    libcurl4-openssl-dev \
    libonig-dev  \
    libpq-dev \
    libjpeg62-turbo-dev \
    libfreetype6-dev \
    libpng-dev \
    libjpeg-dev \
    libxml2-dev \
    mc \
    procps \
    nano \
    git \
    && apt-get clean

RUN docker-php-ext-install pdo
RUN docker-php-ext-install pdo_pgsql
RUN docker-php-ext-install mbstring
RUN docker-php-ext-install zip
RUN docker-php-ext-install curl
RUN docker-php-ext-install json
RUN docker-php-ext-configure gd --with-jpeg --with-freetype
RUN docker-php-ext-install -j$(nproc) gd
RUN docker-php-ext-install opcache
RUN docker-php-ext-install xml
RUN docker-php-ext-install pgsql && docker-php-ext-enable pgsql
RUN pecl install xdebug && docker-php-ext-enable xdebug

RUN locale-gen en_US.UTF-8 ru_RU.UTF-8

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

RUN touch /var/log/cron.log

ENV TZ=Europe/Moscow
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN echo 'source /var/www/app/.env' >> ~/.bashrc

WORKDIR /var/www/app

EXPOSE 9000
CMD php-fpm

